set :s_user, 'Babar'

server 'dime.ibabar.com',
       user: fetch(:s_user),
       roles: %w{app db web},
       ssh_options: { port: 23554 }