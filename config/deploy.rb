# config valid only for current version of Capistrano
lock '3.4.0'

set :application, 'CrossBooks'
set :repo_url, 'https://gitlab.com/babar/CrossBooks.git'

set :user, -> {fetch(:s_user, 'Babar')}

set :deploy_to, "/home/#{fetch(:user)}/Sites/#{fetch(:application)}"

set :rbenv_type, :user # or :system, depends on your rbenv setup
set :rbenv_ruby, File.read('.ruby-version').strip.sub('ruby-', '')

set :log_level, :info

set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/secrets.yml')

set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system', 'public/uploads')

namespace :deploy do
  desc 'Upload Nginx Conf and restart'
  task :nginx_conf do
    on roles(:app) do
      upload_erb 'config/deploy/nginx.conf.erb', "#{shared_path}/config/nginx.conf"
      execute :sudo, 'service nginx restart'
    end
  end
end
