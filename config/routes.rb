Rails.application.routes.draw do
  devise_for :customers,
             path_names: {sign_in: 'login', sign_out: 'logout', sign_up: 'register'},
             controllers: { registrations: 'registrations' }

  devise_scope :customer do
    get 'login', to: 'devise/sessions#new'
    get 'register', to: 'devise/registrations#new'
    delete 'logout', to: 'devise/sessions#destroy'
  end

  root 'shops#show'

  resource :profile, only: :show
  resource :shop, only: :show do
    get 'cart'
  end

  namespace :admin do
    get '/', to: 'products#index'
    resources :products, except: :show
    devise_for :admins, path: 'auth',
               path_names: {sign_in: 'login', sign_out: 'logout'}, as: :auth, singular: :admin,
               controllers: {sessions: 'devise/sessions'}, class_name: 'Admin'
  end

  namespace :api do
    resources :orders, only: :index
    resources :products, only: :index
    resource :me, only: :show, controller: :customers
    resources :charges, only: :create
  end
end
