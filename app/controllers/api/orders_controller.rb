class Api::OrdersController < BaseController
  def index
    render json: orders_hash
  end

  private
    def orders_hash
      current_customer.orders.includes(:items).map{|o| {order_no: o.order_no, total: (sprintf '%.2f', o.total), date: o.created_at, items_count: o.items_count}}
    end

end