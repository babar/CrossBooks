class Api::ProductsController < BaseController
  skip_before_action :authenticate_customer!, only: :index

  def index
    render json: products_hash
  end

  private
    def products_hash
      # Product.all.map{|o| {name: o.order_no, price: (sprintf '%.2f', o.price), description: o.description, status: o.status}}
      Product.all
    end

end