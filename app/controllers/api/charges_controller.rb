class Api::ChargesController < BaseController

  def create
    p transaction_url
    products = Product.find(params[:items])

    response = charge_card(products.sum(&:price))

    if response['success']
      order = Order.create!(customer: current_customer)
      products.each {|p| order.items.create(product: p)}
    end

    render json: response
  end

  private
    def charge_card(amount)
      data = {card_no: params[:card_no], amount: amount}
      r = Net::HTTP.post_form transaction_url, data

      JSON.parse(r.body)
    end

    def transaction_url
      c = Rails.application.config.payment_gateway
      uri = URI('%s/transactions' % c[:url])
      uri.user = c[:username]
      uri.password = c[:password]
      uri
    end

end