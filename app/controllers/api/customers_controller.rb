class Api::CustomersController < BaseController
  def show
    render json: current_customer
  end
end