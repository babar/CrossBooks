class ShopsController < BaseController
  skip_before_action :authenticate_customer!, only: :show

  def show
  end
end