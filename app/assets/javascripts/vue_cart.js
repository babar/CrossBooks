
$(function() {
    var el = '#cart';
    if (!$(el).length) return;

    var cart = new Vue({
        el: el,

        data: {
            products: [],
            cart_items: Cookies.getJSON('_cart') || [],
            show_payment: false,
            user: {},
            error_message: '',
            shopping_done: false
        },

        created: function() {
            this.fetch_products();
            this.fetch_user()
        },

        methods: {
            fetch_products: function() {
                var self = this;
                $.get('/api/products', function(data) {
                    self.products = data.map(function(p) {
                        p.price = parseFloat(p.price);
                        return p;
                    });
                });
            },

            fetch_user: function() {
                var self = this;
                $.get('/api/me', function(data) {
                    self.user = data
                });
            },

            remove_from_cart: function(id) {
                var i = this.cart_items.indexOf(id);
                if (i != -1){ this.cart_items.splice(i, 1) }
            },

            create_order: function(e) {
                e.preventDefault();
                var self = this;
                $.post(
                    '/api/charges',
                    {
                        card_no: $('#card_no').val(),
                        items: this.cart_items
                    },
                    function(data) {
                        if (data.success) {
                            self.cart_items = [];
                            self.shopping_done = true
                        } else {
                            self.error_message = data.error_message;
                        }
                    }
                )
            }
        },

        computed: {
            items: function() {
                var self = this;
                return this.cart_items.map(function(item) {
                    return self.products.find(function(product) {return product.id == item});
                });
            },

            total_price: function() {
                return this.items
                    .map(function(p) {return p.price})
                    .reduce(function(prev, curr) {return (prev + curr)}, 0);
            }
        }
    });

    cart.$watch('cart_items', function(val) {
        $('.nav-cart-items').text(val.length);
        Cookies.set('_cart', val);
    });
});