$(function() {
    var el = '#orders';
    if (!$(el).length) return;

    new Vue({
        el: el,

        data: {
            columns: ['order_no', 'total', 'items_count', 'date'],
            orders: [],
            sortKey: '',
            sort: 1
        },

        created: function() {
            this.fetch_orders()
        },

        methods: {
            fetch_orders: function() {
                var self = this;
                $.get('/api/orders', function(data) {
                    self.orders = data.map(function(o) {
                        o.total = parseFloat(o.total);
                        o.date = new Date(o.date);
                        return o;
                    });
                })
            },

            sortBy: function (key) {
                this.sort = ((this.sortKey == key)? (this.sort * -1) : 1);
                this.sortKey = key;
            },

            sort_class: function(column) {
                return (this.sortKey == column) && ((this.sort == 1)? 'asc' : 'dsc');
            }
        }
    });
});