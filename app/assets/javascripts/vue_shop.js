$(function() {
    var el = '#products';
    if (!$(el).length) return;

    var shop = new Vue({
        el: el,

        data: {
            columns: ['name', 'description', 'price'],
            products: [],
            search: '',
            sortKey: 'name',
            sort: 1,
            cart: Cookies.getJSON('_cart') || []
        },

        created: function() {
            this.fetch_products();
            //if(Cookies.getJSON('_cart')) this.cart = Cookies.getJSON('_cart');
        },

        methods: {
            fetch_products: function() {
                var self = this;
                $.get('/api/products', function(data) {
                    self.products = data.map(function(p) {
                        p.price = parseFloat(p.price);
                        return p;
                    });
                })
            },

            sortBy: function (key) {
                this.sort = ((this.sortKey == key)? (this.sort * -1) : 1);
                this.sortKey = key;
            },

            sort_class: function(column) {
                return (this.sortKey == column) && ((this.sort == 1)? 'asc' : 'dsc');
            },

            add_to_cart: function(id) {
                this.cart.push(id)
            },

            remove_from_cart: function(id) {
                var i = this.cart.indexOf(id);
                if (i != -1){ this.cart.splice(i, 1) }
            },

            in_cart: function(id) {
                return (this.cart.indexOf(id) != -1)
            }

        }
    });

    shop.$watch('cart', function(val) {
        $('.nav-cart-items').text(val.length);
        Cookies.set('_cart', val);
    });
}());