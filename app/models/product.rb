class Product < ApplicationRecord
  validates :name, presence: true, uniqueness: true
  validates_presence_of :description, :price
  validates :status, presence: true, inclusion: {in: [0, 1]}

  after_initialize do
    self.status ||= 1
  end
end
