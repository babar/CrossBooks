class OrderItem < ApplicationRecord
  belongs_to :order
  belongs_to :product

  validates_presence_of :order, :product

  before_validation do
    self.unit_price = product.price if self.unit_price == 0.0 && product.present?
    self.total_price = self.unit_price * self.qty
  end

  after_commit :update_total_of_order

  private
    def update_total_of_order
      self.order.recalculate_total!
    end
end
