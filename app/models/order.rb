class Order < ApplicationRecord
  belongs_to :customer
  has_many :items, class_name: OrderItem

  validates_presence_of :customer

  before_validation :generate_order_no


  def date
    created_at.strftime('%-d/%-m/%Y')
  end

  def recalculate_total!
    update_attribute(:total, items.sum(&:total_price))
  end

  def items_count
    items.size
  end

  private
    def generate_order_no
      while order_no.nil?
        random_token = SecureRandom.uuid.first(6)
        self.order_no = random_token unless self.class.exists?(order_no: random_token)
      end
    end
end
