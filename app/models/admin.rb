class Admin < ApplicationRecord
  # Include default devise modules. Others available are: , :trackable, :validatable
  # :confirmable, :lockable, :timeoutable and :omniauthable :registerable,  :recoverable,

  devise :database_authenticatable, :rememberable
end
