module ApplicationHelper

  def bootstrap_devise_error_messages!
    return '' if resource.errors.empty?

    messages = resource.errors.full_messages.map { |msg| content_tag(:li, msg) }.join

    html = <<-HTML
    <div class="alert alert-warning alert-block">
      <button type="button" class="close" data-dismiss="alert">&times;</button>
      <h5>There are some errors</h5>
      <ul>#{messages}</ul>
    </div>
    HTML

    html.html_safe
  end

  def cart_items
    JSON.parse (cookies[:_cart] || '[]')
  end
end
