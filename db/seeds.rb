require 'factory_girl_rails'
require 'faker'

puts 'Seed Customers'

FactoryGirl.create(:customer)

9.times do
  FactoryGirl.create(:customer, :dyn)
end

puts 'Seeding products'


books = ['Jacob Have I Loved', 'I Will Fear No Evil', 'Some Buried Caesar', 'Mother Night', 'The Waste Land', 'Gone with the Wind', 'As I Lay Dying', 'When the Green Woods Laugh', 'To Your Scattered Bodies Go', "The Stars' Tennis Balls", 'Cabbages and Kings', 'Bury My Heart at Wounded Knee', 'Consider the Lilies', 'A Summer Bird-Cage', 'Beyond the Mexique Bay', 'A Time of Gifts', 'Quo Vadis', 'An Evil Cradling', 'Antic Hay', 'The Green Bay Tree', 'His Dark Materials', 'For Whom the Bell Tolls', 'Sleep the Brave', 'Those Barren Leaves, Thrones, Dominations', 'No Country for Old Men', 'Of Human Bondage', 'Lilies of the Field', 'Look to Windward', "All the King's Men", 'The Doors of Perception', 'Ah, Wilderness!', 'The Sun Also Rises', 'The Moving Toyshop', 'The Road Less Traveled', 'Nectar in a Sieve', 'Everything is Illuminated', 'The Far-Distant Oxus', 'That Good Night', "What's Become of Waring", 'Such, Such Were the Joys', 'By Grand Central Station I Sat Down and Wept', 'Eyeless in Gaza', 'The Curious Incident of the Dog in the Night-Time', "The Soldier's Art", 'After Many a Summer Dies the Swan', "It's a Battlefield", 'Where Angels Fear to Tread', 'Vanity Fair', 'Vile Bodies', 'The Glory and the Dream', 'The Grapes of Wrath', "The Mirror Crack'd from Side to Side", 'Carrion Comfort', "Blood's a Rover", 'Death Be Not Proud', 'A Glass of Blessings', 'The Yellow Meads of Asphodel', 'Beneath the Bleeding', 'A Scanner Darkly', 'The Man Within', 'The Cricket on the Hearth', 'The Moon by Night', 'I Know Why the Caged Bird Sings', 'Of Mice and Men', 'Blithe Spirit', 'The Heart Is Deceitful Above All Things', 'Butter In a Lordly Dish', 'A Catskill Eagle', 'Ego Dominus Tuus', 'Tirra Lirra by the River', 'Precious Bane', 'Great Work of Time', 'Brandy of the Damned', 'To Sail Beyond the Sunset', 'Nine Coaches Waiting', 'Wildfire at Midnight', 'Noli Me Tangere', 'No Longer at Ease', 'Ring of Bright Water', 'Time To Murder And Create', 'nfinite Jest', 'I Sing the Body Electric', 'The Wealth of Nations', 'Dance Dance Dance', 'Shall not Perish', 'Waiting for the Barbarians', 'The Violent Bear It Away', 'In Death Ground']

while Product.count < 30 do
  FactoryGirl.build(:product, name: books.sample).save
end

puts 'Seeding Orders'

Customer.all.each do |customer|
  rand(1..15).times do
    FactoryGirl.create(:order, customer: customer)
  end
end

puts 'Seeding Order items'

products = Product.all
Order.all.each do |order|
  rand(1..4).times do
    FactoryGirl.create(:order_item, order: order, product: products.sample)
  end
end