class CreateOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :orders do |t|
      t.string :order_no
      t.references :customer, index: true, foreign_key: true
      t.decimal :total, default: 0, precision: 16, scale: 2

      t.timestamps
    end
  end
end
