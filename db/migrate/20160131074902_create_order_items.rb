class CreateOrderItems < ActiveRecord::Migration[5.0]
  def change
    create_table :order_items do |t|
      t.references :order, index: true, foreign_key: true
      t.references :product, index: true, foreign_key: true
      t.integer :qty, default: 1
      t.decimal :unit_price, default: 0, precision: 16, scale: 2
      t.decimal :total_price, default: 0, precision: 16, scale: 2

      t.timestamps
    end
  end
end
