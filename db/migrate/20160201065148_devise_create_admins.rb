class DeviseCreateAdmins < ActiveRecord::Migration
  def migrate(direction)
    super

    if direction == :up
      Admin.create!(email: 'admin@example.com', password: 'Hello123', password_confirmation: 'Hello123')
    end
  end

  def change
    create_table :admins do |t|
      t.string :email,              null: false, default: ""
      t.string :encrypted_password, null: false, default: ""

      t.datetime :remember_created_at

      t.timestamps null: false
    end

    add_index :admins, :email,                unique: true
  end
end
