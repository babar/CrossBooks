class CreateProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :products do |t|
      t.string :name
      t.decimal :price, default: 0, precision: 16, scale: 2
      t.text :description
      t.integer :status, limit: 1, default: 1

      t.timestamps
    end
  end
end
