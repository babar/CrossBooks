describe Product, type: :model do
  it {should validate_presence_of :name}
  it {should validate_uniqueness_of :name}
  it {should validate_presence_of :description}
  it {should validate_presence_of :price}
  it {should validate_inclusion_of(:status).in_array([0, 1])}
end