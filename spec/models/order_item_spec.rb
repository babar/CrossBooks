describe OrderItem, type: :model do
  it {should validate_presence_of :order}
  it {should validate_presence_of :product}
  it {should belong_to :product}
  it {should belong_to :order}

  it 'automatically calculate total price' do
    item = create(:order_item, qty: 5)
    expect(item.total_price).to eql(5*item.product.price)
  end
end