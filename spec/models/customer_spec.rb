describe Customer, type: :model do
  it {should validate_presence_of :first_name}
  it {should validate_presence_of :last_name}
  it {should validate_presence_of :email}
  it {should validate_length_of(:password).is_at_least(7)}

  it {should have_many(:orders)}
end