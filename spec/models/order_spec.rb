describe Order, type: :model do
  it {should validate_presence_of :customer}
  it {should belong_to(:customer)}
  it {should have_many :items}

  describe '#date' do
    it 'return formatted date' do
      order = create(:order, created_at: Time.utc(2016,01,31))
      expect(order.date).to be_eql('31/1/2016')
    end
  end

  describe '#order_no' do
    it 'generate order_no automatically' do
      order = Order.new
      order.valid?
      expect(order.order_no).not_to be_nil
    end
  end

  describe '#total' do
    it 'automatically updates grand total of a order based on items' do
      order = create(:order)
      expect(order.total).to eq(0.00)

      items = [create(:order_item, order: order)]
      order.items << items.last
      expect(order.total).to eql(items.last.total_price)

      3.times {
        items << create(:order_item, order: order)

        order.items << items.last
        expect(order.total).to eql(items.map(&:total_price).sum)
      }
    end
  end
end