FactoryGirl.define do
  factory :order_item do
    order {Order.first || create(:order)}
    product {create(:product)}
    qty {rand(1..10)}
  end
end