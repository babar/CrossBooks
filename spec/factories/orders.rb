FactoryGirl.define do
  factory :order do
    customer {Customer.first || create(:customer)}
    total 0
  end
end