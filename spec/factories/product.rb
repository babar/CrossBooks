require 'faker'

FactoryGirl.define do
  factory :product do
    name {Faker::Book.title}
    price {rand(1.0..30.0).round(2)}
    description {"#{name} by #{Faker::Book.author}. Published by #{Faker::Book.publisher} in #{Faker::Book.genre}."}
    status {[0, 1].sample}
  end
end