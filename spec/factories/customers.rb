require 'faker'

FactoryGirl.define do
  factory :customer do
    first_name 'Jone'
    last_name 'Doe'
    email 'john@example.com'
    password 'TesTest1'
    password_confirmation 'TesTest1'

    trait :dyn do
      first_name {Faker::Name.first_name}
      last_name {Faker::Name.last_name}
      email {Faker::Internet.safe_email(first_name)}
    end
  end
end