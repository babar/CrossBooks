describe 'register process', type: :feature do
  before(:each) {Customer.destroy_all}

  it 'registers user with valid details' do
    visit root_path
    click_on 'Register'
    fill_in 'First name', with: 'John'
    fill_in 'Last name', with: 'Doe'
    fill_in 'Email', with: 'demo@example.com'
    fill_in 'Password', with: 'TestTest1'
    fill_in 'Password confirmation', with: 'TestTest1'

    click_button 'Sign up'

    expect(page).to have_content 'Logout'
    expect(page).to have_content 'Welcome John'
    expect(page).not_to have_content 'Login'
    expect(page).not_to have_content 'Register'
  end

  it ' does not register with invalid details' do
    visit root_path
    click_on 'Register'
    fill_in 'Last name', with: 'Doe'
    fill_in 'Email', with: 'demo@example.com'
    fill_in 'Password', with: 'TestTest1'
    fill_in 'Password confirmation', with: 'TestTest1'

    click_button 'Sign up'

    expect(page).to have_content 'error'
    expect(page).not_to have_content 'Logout'
    expect(page).to have_content 'Register'
  end
end