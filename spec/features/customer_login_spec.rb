describe 'Login process', type: :feature do
  before(:all) do
    Customer.create!(first_name: 'John', last_name: 'Doe', email: 'demo@example.com', password: 'TestTest1', password_confirmation: 'TestTest1')
  end

  it 'Login user with valid credential' do
    visit root_path
    click_on 'Login'
    fill_in 'Email', with: 'demo@example.com'
    fill_in 'Password', with: 'TestTest1'

    click_button 'Sign in'

    expect(page).to have_content 'Logout'
    expect(page).to have_content 'Welcome John'
    expect(page).not_to have_content 'Login'
    expect(page).not_to have_content 'Register'
  end

  it 'does not login with invalid credential' do
    visit root_path
    click_on 'Login'
    fill_in 'Email', with: 'demo@example.com'
    fill_in 'Password', with: 'Wrong!'

    click_button 'Sign in'

    expect(page).to have_content 'Invalid email or password'
    expect(page).not_to have_content 'Logout'
    expect(page).to have_content 'Register'
  end
end